%-------------------------------------------------------------------------------
% This file provides a skeleton ATLAS note.
\pdfinclusioncopyfonts=1
% This command may be needed in order to get \ell in PDF plots to appear. Found in
% https://tex.stackexchange.com/questions/322010/pdflatex-glyph-undefined-symbols-disappear-from-included-pdf
%-------------------------------------------------------------------------------
% Specify where ATLAS LaTeX style files can be found.
\RequirePackage{latex/atlaslatexpath}
% You can comment out the above line if the files are in a central location, e.g. $HOME/texmf.
%-------------------------------------------------------------------------------
\documentclass[NOTE, REPORT=true, atlasdraft=true, UKenglish]{atlasdoc}
% The language of the document must be set: usually UKenglish or USenglish.
% british and american also work!
% Commonly used options:
%  atlasdraft=true|false This document is an ATLAS draft.
%  texlive=YYYY          Specify TeX Live version (2020 is default).
%  NOTE                  The document is an ATLAS note (draft).
%  REPORT=true|false     Use scrreprt|scrartcl as main class for document.
%  txfonts=true|false    Use txfonts rather than the default newtx.
%  paper=a4|letter       Set paper size to A4 (default) or letter.

%-------------------------------------------------------------------------------
% Extra packages:
\usepackage[minimal]{atlaspackage}
% Commonly used options:
%  backend=bibtex        Use the bibtex backend rather than biber.
%  subfigure|subfig|subcaption  to use one of these packages for figures in figures.
%  minimal               Minimal set of packages.
%  default               Standard set of packages.
%  full                  Full set of packages.
%-------------------------------------------------------------------------------
% Style file with biblatex options for ATLAS documents.
\usepackage{atlasbiblatex}
% Commonly used options:
%  backref=true|false    Turn on or off back references in the bibliography.

% Package for creating list of authors and contributors to the analysis.
\usepackage{atlascontribute}
\usepackage{multirow}
\usepackage{booktabs}

% \setkeys{Gin}{width=8cm}

% Useful macros
\usepackage{atlasphysics}
% See doc/atlas_physics.pdf for a list of the defined symbols.
% Default options are:
%   true:  journal, misc, particle, unit, xref
%   false: BSM, hepparticle, hepprocess, hion, jetetmiss, math, process,
%          other, snippets, texmf
% See the package for details on the options.

% Macro to add to-do notes (for several authors). Uses the todonotes package.
% \ATLnote{JS}{Jane}{green!20}{green!50!black!60}
% add macros \JSnote and \JSinote for notes in the margin and inline.
% The first colour is for the body and the second for the border of the note.
% Set output=false in order not to print out the notes.
% Set shift=false to avoid adjustment of margins.
% Check for notes still left by commenting out the package in the final version of the note.
\usepackage[output=true, shift=flase]{atlastodo}

% Files with references for use with biblatex.
% Note that biber gives an error if it finds empty bib files.
% \addbibresource{ATLIDTRKCP-427-INT.bib}
\addbibresource{bib/ATLAS.bib}
\addbibresource{bib/CMS.bib}
\addbibresource{bib/ConfNotes.bib}
\addbibresource{bib/PubNotes.bib}

% Paths for figures - do not forget the / at the end of the directory name.
\graphicspath{{logos/}{figures/}}

% Add your own definitions here (file ATLIDTRKCP-427-INT-defs.sty).
\usepackage{ATLIDTRKCP-427-INT-defs}

%-------------------------------------------------------------------------------
% Generic document information.
%-------------------------------------------------------------------------------

% Title, abstract and document.
\input{ATLIDTRKCP-427-INT-metadata}
% Author and title for the PDF file.
\hypersetup{pdftitle={ATLAS document},pdfauthor={The ATLAS Collaboration}}

%-------------------------------------------------------------------------------
% Content
%-------------------------------------------------------------------------------
\begin{document}

\maketitle

\tableofcontents

% List of contributors - print here or after the Bibliography.
% \PrintAtlasContribute{0.30}
% \clearpage

% List of to-do notes.
% \listoftodos

%-------------------------------------------------------------------------------
\chapter{Introduction}\label{sec:intro}
%-------------------------------------------------------------------------------

This note presents prospective calculations of the inner-detector (ID) track reconstruction using simulated proton-proton collisions at $\sqrt{s}=14$ TeV. We emphasize on the performance of the Run 2 tracking level cuts on a Run 4 Monte-Carlo (MC), with an aim of providing recommendations for software tracking in the future. The dataset is a MC consisting of 95000 $pp\to t\overline{t}$ events produced using Powheg+Pythia8\footnote{The sample tag is \texttt{\tiny mc21\_14TeV.601229.PhPy8EG\_A14\_ttbar\_hdamp258p75\_SingleLep.merge.AOD.e8481\_s4149\_r14701\_r14702}}. This MC is then processed using release 23 of the ATLAS offline analysis software, specifically the Physics Monitoring program \texttt{InDetPhysValMonitoring}\footnote{Code found at {\tiny \url{https://gitlab.cern.ch/atlas/athena/-/tree/main/InnerDetector/InDetValidation/InDetPhysValMonitoring}}} (IDPVM). This MC is produced using the layout of the new all Silicon Inner-Tracker (ITk) in order to fairly characterize the Run 4 environment. The overall goal of this project comes in two major parts. Initially, the performance of the current working points is established. While not explicitly discussed here, this performance can be seen in the performance of the non-optimized TightPrimary and Loose cut levels throughout the figures. Once the specific needs were characterized, ITk-specific Loose and Tight selections were developed, which should be optimized for the new ITk geometry

%1. Characterize the ITk track reconstruction efficiency and fake rate using only the reconstruction cuts, as well as applying existing track selection criteria on top
%2. Investigate ITk-specific Loose and Tight selections specifically for ITk geometry, to be applied on top of the reconstruction cuts

The most important heuristics to take into account here are the reconstruction efficiencies and fake rate. The fake rate is the fraction of tracks which either do not have a corresponding true track or have less than half of their hits matching a truth track. The efficiency is similarly calculated, however it only takes into account the tracks which have a corresponding truth track. A track counts towards the efficiency if its matching probability is greater than 50\%. The matching probability is a weighted sum over hits in the track, with pixel clusters having a weight of 2 (as they are 2D measurements) and the strip clusters have a weight of 1 (1D measurement). Together these quantities give a good indication of how well a particular set of cuts perform, as they will change when the requirements for a passing/failing track change. More details on specific formulae for these quantities can be found in various ATLAS notes such as \cite{ATL-PHYS-PUB-2015-051}

%-------------------------------------------------------------------------------
\chapter{Analysis}\label{sec:analysis}
%-------------------------------------------------------------------------------

Based on initial readouts of this sample within IDPVM\footnote{These results are essentially seen in all figures reading from the non-optimized Loose \& TightPrimary}, there is significant loss in efficiency and an increase in fake rate specifically in the new expanded forward region of the ITk, for which the previous cut levels were not optimized. In order to determine which cuts could be modified to recover lost efficiencies, a systematic approach is taken. For each variable listed in Table~\ref{tab:cuts}, the cuts were arbitrarily modified by one unit, if the modification of a cut showed an increase in fake rate or a decrease in efficiency, the corresponding cut level is either left the same as before, or changed in the opposite direction in the final recommendation.

\begin{table}[ht]
    \centering
    \begin{tabular}{@{}lcc@{}}
        \toprule
        Requirement & Loose & TightPrimary \\ \midrule
        Max $|\eta|$ & 2.5 & 2.5 \\ \midrule
        Min $|\eta|$ for Strict Si Hits & N/A & 1.65 \\ \midrule
        Minimum Hits (All Si) & 8 & 9 \\ \midrule
        Minimum Hits (Innermost) & N/A & 1 \\ \midrule
        Maximum Holes (All Si) & 2 & 2 \\ \midrule
        Maximum Holes (Pixel Layers) & 1 & 0 \\ \bottomrule
    \end{tabular}
    \vspace{10pt}
    \caption{Summary of the Loose and TightPrimary cut levels for Run-2. The strict $\eta$ hits cut is in reference to a split in the behavior of the Minimum Hits (all Si), below this $\eta$ value, the default value is used, if above this $\eta$ value, the ``strict'' value is used. A ``hole'' refers to a hit that the tracking algorithm believes should be there, but is not there in reality.}
    \label{tab:cuts}
\end{table}
There is also the specific requirements for track reconstruction, which are applied before tracking is done, detailed in Table \ref{tab:reco}.
\begin{table}[ht]
\centering
\begin{tabular}{c|ccc}
\hline
\multirow{2}{*}{Requirements} & \multicolumn{3}{c}{Pseudorapidity interval} \\ \cline{2-4} 
 & $|\eta|<2.0$ & $2.0<|\eta|<2.6$ & $2.6<|\eta|<4.0$ \\ \hline
Pixel + Strip hits & $\geq9$ & $\geq8$ & $\geq7$ \\ \hline
Pixel hits & $\geq1$ & $\geq1$ & $\geq1$ \\ \hline
holes & $\leq 2$ & $\leq 2$ & $\leq 2$ \\ \hline
$p_T$ [MeV] & >900 & >400 & >400 \\ \hline
$|d_0|$ [mm] & $\leq 2.0$ & $\leq 2.0$ & $\leq 2.0$ \\ \hline
$|z_0|$ [cm] & $\leq 20.0$ & $\leq 20.0$ & $\leq 20.0$ \\ \hline
\end{tabular}
\vspace{10pt}
\caption{Track reconstruction requirements.}
\label{tab:reco}
\end{table}
\section{Studies on TightPrimary}
A separate study was done on each of TightPrimary and Loose, the takeaways from the TightPrimary study were that:
\begin{itemize}
    \item Removing the Strict Si Hit behavior resulted in an increase of up to 15\% in efficiency while negligible impact on the fake rate.
    \item Increasing the max number of allowed holes in the Pixel layers doubles the fake rate with a 10-20\% gain in efficiency, which is undesirable.
    \item Modifying the number of hits above the strict $\eta$ cutoff has a similar effect to just removing the behavior entirely, though the efficiency gains are $\sim 5\%$ better if this cut is removed entirely\footnote{See Appendex A.1 for plots}.
    \item Changing the requirement on having hits in the Innermost Si layers changes nothing at all.
\end{itemize}
The end result of these studies are the optimized TightPrimary seen in Table~\ref{tab:opttp}, in comparison to TightPrimary as it currently exists.

\begin{table}[ht]
    \centering
    \begin{tabular}{@{}lcc@{}}
        \toprule
        Requirement & TightPrimary & Optimized TightPrimary \\ \midrule
        Max $|\eta|$ & 2.5 & 4.0 \\ \midrule
        Min $|\eta|$ for Strict Si Hits & 1.65 & N/A \\ \midrule
        Minimum Hits (All Si) & 9 & 10 \\ \midrule
        Minimum Hits (Innermost) & 1 & N/A \\ \midrule
        Maximum Holes (All Si) & 2 & 1 \\ \midrule
        Maximum Holes (Pixel Layers) & 0 & 0 \\ \bottomrule
        \end{tabular}
    \vspace{10pt}
    \caption{Comparison of Optimized TightPrimary with the original.}
    \label{tab:opttp}
\end{table}

Note the $|\eta|$ requirement for the previous version of TightPrimary is automatically changed to 4.0 when a Run 4 sample is provided to guarantee all track data is used.

\section{Studies on Loose}
The results for the same study on the Loose cut level are:
\begin{itemize}
    \item Changing max number of allowed holes in Si layers has minimal effect on fake rate.
    \item Changing number of allowed holes in the Pixel layer specifically did have a noticeable change in the fake rate.
    \item Changing the min number of required hits only makes a difference in the $|\eta|>2.0$ region.
\end{itemize}
% add reference to https://cds.cern.ch/record/2776651/files/ATL-PHYS-PUB-2021-024.pdf
These points together raised a problem when further inspected. It is important to note that the Loose/TightPrimary requirement is not the only one imposed on the tracks within IDPVM. In addition to Loose, there are initial reconstruction requirements imposed, which are further discussed in \cite{ATL-PHYS-PUB-2021-024}, summarized in Table~\ref{tab:reco}. 

Importantly, the requirements we have imposed for the Loose cut level are either less strict or the same as cuts required for track reconstruction. This means the tracks we are already processing within IDPVM will pass these cuts 100\% of the time, making Loose completely redundant. There are two possible solutions to this: 
\begin{itemize}
    \item Remove Loose and in its place use the bare reconstruction requirements as the default tracking cut use case.
    \item Shift TightPrimary to be the new ''Loose`` and make a new ''TightPrimary`` which is even stricter and has even lower fake rates.
\end{itemize}
For the purposes of this project, a tightened version of Loose is still presented, though the differences between the old and optimized Loose are minimal enough such that the first option seems to be more desirable.

The ending results of these studies in comparison to Loose is seen in Table~\ref{tab:optloose}.

\begin{table}[ht]
    \centering
    \begin{tabular}{@{}lcc@{}}
        \toprule
        Requirement & Loose & Opt. Loose \\ \midrule
        Max $|\eta|$ & 2.5 & 4.0 \\ \midrule
        Minimum Hits (All Si) & 8 & 9 \\ \midrule
        Maximum Holes (All Si) & 2 & 1 \\ \midrule
        Maximum Holes (Pixel Layers) & 1 & 1 \\ \bottomrule
    \end{tabular}
    \vspace{10pt}
    \caption{Comparison of Optimized Loose with the original.}
    \label{tab:optloose}
\end{table}

Note the $|\eta|$ requirement for the previous version of Loose is automatically changed to 4.0 when a Run 4 sample is provided to guarantee all track data is used.

%-------------------------------------------------------------------------------
\chapter{Results}\label{sec:result}
%-------------------------------------------------------------------------------

There are several variables of interest to consider for these studies. The most important plots are the reconstruction efficiency and fake rates. IDPVM provides histograms for efficiency and fake rates against $\eta$, $p_T$ $\phi$, $d_0$, $z_0$, and several others. The most important plots to consider are the plots against $\eta$ and $p_T$. For the HL-LHC runs, the $p_T$ and $\eta$ distributions will have large discrepancies compared to those for run 2, so they are most useful when characterizing performance for the working points. Another consideration to make is which set of tracks we analyze. By default IDPVM will only analyze tracks from the hard-scatter vertex, but due to the increased pileup environment of the HL-LHC it is worth it to study the tracking performance of the other primary vertices. These plots where the tracks from Pileup are considered are labeled as such.

Figures \ref{fig:effs_eta} and \ref{fig:effs_pt} show the efficiencies of the optimized TightPrimary and Loose cut levels in comparison to how they currently exist. 
The performance of optimized TightPrimary shows great promise in the region where $|\eta|>2.6$, a region where the current cut levels clearly struggle. This difference is even greater in the pileup plots where the current cut levels struggle more, there is a gain of $10\%$ throughout the region, where in the Hard Scatter plots this is a bit of a steeper increase to that maximum. 
The optimized loose is not as promising unfortunately, there are minimal losses in efficiency for both Hard Scatter and Pile Up tracks. This definitely grants some credence to the idea of replacing Loose with the current set of reconstruction cuts.

\begin{figure}[ht]
    \centering
    \includegraphics[width=8cm]{figs/Efficiency_HardScatter_efficiency_vs_eta.pdf}
    \includegraphics[width=8cm]{figs/Efficiency_PileUp_efficiency_vs_eta.pdf}
    \caption{Hard-Scatter (left) and pileup (right) Efficiencies vs $\eta$. Ratio is respect to the No Cut data, which is just the reconstruction cuts}
    \label{fig:effs_eta}
\end{figure}

\begin{figure}[ht]
    \centering
    \includegraphics[width=8cm]{figs/Efficiency_HardScatter_efficiency_vs_pt.pdf}
    \includegraphics[width=8cm]{figs/Efficiency_PileUp_efficiency_vs_pt.pdf}
    \caption{Hard-Scatter (left) and pileup (right) Efficiencies vs $p_T$. Ratio is respect to the No Cut data, which is just the reconstruction cuts}
    \label{fig:effs_pt}
\end{figure}

The plots in Figures \ref{fig:fakes_eta} and \ref{fig:fakes_pt} show the linked and unlinked fake tracks. The difference is simply the origin of these fakes, the unlinked fake rates do not have a linked truth track, while the other fake tracks do. There are several studies done by the Upgrade Tracking group which say that these should both behave similarly.
% The fakes for optimized TightPrimary are mostly similar (to the original) at low $p_T$, but the more important region in high $p_T$ are lower, which is especially important for HL-LHC, even if the difference is only by a few percent. 
Optimized Loose shines a bit more here, with the fake rates consistently lower than those for the current cut level, though it is unclear as to whether or not it is enough to justify the overall loss in efficiency. 

\begin{figure}[ht]
    \centering
    \includegraphics[width=8cm]{figs/FakeRate_HardScatter_fakerate_vs_eta.pdf}
    \includegraphics[width=8cm]{figs/Unlinked_Fake_HardScatter_unlinked_fakerate_vs_eta.pdf}
    \caption{Regular (left) and Unlinked (right) Fake Rates vs $\eta$. Ratio is respect to the No Cut data, which is just the reconstruction cuts}
    \label{fig:fakes_eta}
\end{figure}

\begin{figure}[ht]
    \centering
    \includegraphics[width=8cm]{figs/FakeRate_HardScatter_fakerate_vs_pt.pdf}
    \includegraphics[width=8cm]{figs/Unlinked_Fake_HardScatter_unlinked_fakerate_vs_pt.pdf}
    \caption{Regular (left) and Unlinked (right) Fake Rates vs $p_T$. Ratio is respect to the No Cut data, which is just the reconstruction cuts}
    \label{fig:fakes_pt}
\end{figure}

Finally, the impact of the new cut levels on the resolution of the $d_0$ and $z_0$ impact parameters is investigated. These distributions are shown in Figure \ref{fig:resos}. The comparisons show a small impact on the, but since the overall shape remains the same, there is no cause for concern. 

\begin{figure}
    \centering
    \includegraphics[width=8cm]{figs/Reso_D0_HardScatter.pdf}
    \includegraphics[width=8cm]{figs/Reso_Z0_HardScatter.pdf}
    \caption{Resolution plots of $d_0$ (left) and $d_0$ (right). Ratio is respect to the No Cut data, which is just the reconstruction cuts}
    \label{fig:resos}
\end{figure}

% All figures and tables should appear before the summary and conclusion.
% The package placeins provides the macro \FloatBarrier to achieve this.
% \FloatBarrier

%-------------------------------------------------------------------------------
\chapter{Conclusion}\label{sec:conclusion}
%-------------------------------------------------------------------------------

This work is very important for the future runs of the LHC at ATLAS, good reconstruction efficiency and low fake rates are very important for analyses beyond run 4. The expanded coverage of the inner detector initially showed a bit of concerning losses in reconstruction efficiency and gains in fake rates, but with some loose tuning using existing tool-kits within the ATLAS software package. 

The optimized working point for TightPrimary has shown great performance gains. With some cuts removed and modified, the lost performance was significantly improved upon. The optimized Loose working point has similar gains, but much more minimal. The much more important point is the similarity of the Loose performance to the bare reconstruction level cuts. Further modified reconstruction cuts optimized for run 4 would work just as well as the Loose. 

There is certainly some extension to this work as well. An upgrade to the ITk is already planned to add timing information in the forward region. Further investigation of these working points with this information should absolutely be done. Tracking with timing or 4D tracking in general is a big interest for the future of the LHC, so this should be a top priority for future work.

%-------------------------------------------------------------------------------
% If you use biblatex and either biber or bibtex to process the bibliography
% just say \printbibliography here.
\printbibliography{ATLIDTRKCP-427-INT}
% If you want to use the traditional BibTeX you need to use the syntax below.
% \bibliographystyle{obsolete/bst/atlasBibStyleWithTitle}
% \bibliography{ATLIDTRKCP-427-INT,bib/ATLAS,bib/CMS,bib/ConfNotes,bib/PubNotes}
%-------------------------------------------------------------------------------

%-------------------------------------------------------------------------------
% Print the list of contributors to the analysis.
% The argument gives the fraction of the text width used for the names.
%-------------------------------------------------------------------------------
\clearpage
\PrintAtlasContribute{0.30}

%-------------------------------------------------------------------------------
\clearpage
\appendix
\part*{Appendices}
\addcontentsline{toc}{part}{Appendices}
%-------------------------------------------------------------------------------
\chapter{Plots from Optimization Studies}
The following sections reiterate information presented in the following presentations:
\begin{itemize}
    \item {\small \url{https://indico.cern.ch/event/1372820/#30-update-qp-track-selection}}
    \item {\small \url{https://indico.cern.ch/event/1388352/#34-qp-update-track-selections}}
\end{itemize} 
\section{TightPrimary}
For this study, four additional cut levels were implemented for use in IDPVM in order to study the effects of incrementing each cut by a bit, summarized here:
\begin{itemize}
    \item \textbf{No Strict Eta:} Removes the \texttt{minEtaForStrictNSiHitsCut}, meaning only \texttt{minNSiHits} is used over the entire range
    \item \textbf{Si Hits > Cutoff = 10:} This working point (along with the remaining 2) keeps the strict behavior detailed above, and reduces the requirement to 10 hits opposed to 11.
    \item \textbf{MaxPixelHole=1:} Allows for a single hole in hits from pixel clusters
    \item \textbf{InnerMostLayers=0:} Changes the requirement of one hit in the innermost silicon layers.
\end{itemize}
The relevant plots are found in the subsequent figures \ref{fig:tightopteffseta}, \ref{fig:tightopteffspt} \ref{fig:tightoptfakeseta}, and \ref{fig:tightoptfakespt}
\begin{figure}[ht]
    \centering
    \includegraphics[width=8.0cm]{figs/tightopteffetaHS.png}
    \includegraphics[width=8.0cm]{figs/tightopteffetaPU.png}
    \caption{Hard-Scatter (left) and pileup (right) Efficiencies vs $\eta$ for the TightPrimary Optimization. Ratio is with respect to the TightPrimary working point, which is not visible due to the yellow working point being identical.}
    \label{fig:tightopteffseta}
\end{figure}

\begin{figure}[ht]
    \centering
    \includegraphics[width=8.0cm]{figs/tightopteffptHS.png}
    \includegraphics[width=8.0cm]{figs/tightopteffptPU.png}
    \caption{Hard-Scatter (left) and pileup (right) Efficiencies vs $p_T$ for the TightPrimary Optimization. Ratio is with respect to the TightPrimary working point, which is not visible due to the yellow working point being identical.}
    \label{fig:tightopteffspt}
\end{figure}

\begin{figure}[ht]
    \centering
    \includegraphics[width=8.0cm]{figs/tightoptfakeseta.png}
    \caption{Fake Rates vs $\eta$ for the TightPrimary Optimization, Unlinked fakes were not studied here. Ratio is with respect to the TightPrimary working point, which is not visible due to the yellow working point being identical.}
    \label{fig:tightoptfakeseta}
\end{figure}

\begin{figure}[ht]
    \centering
    \includegraphics[width=8.0cm]{figs/tightoptfakespt.png}
    \caption{Fake Rates vs $p_T$ for the TightPrimary Optimization, Unlinked fakes were not studied here. Ratio is with respect to the TightPrimary working point, which is not visible due to the yellow working point being identical.}
    \label{fig:tightoptfakespt}
\end{figure}

\section{Loose}
For this study, 3 additional cut levels were implemented for use in IDPVM in order to study the effects of incrementing each cut by a bit, summarized here:
\begin{itemize}
    \item \textbf{maxNSiHoles$\to$1:} Decrease the maximum number of holes in all silicon layers by 1
    \item \textbf{minNSiHits$\to$9:} Increase the minimum number of hits in all silicon layers by 1
    \item \textbf{maxNPixelHoles$\to$0:} Decrease the maximum number of holes allowed in the pixel layers by 1
\end{itemize}
The relevant plots are found in the subsequent figures \ref{fig:looseopteffseta}, \ref{fig:looseopteffspt} \ref{fig:looseoptfakeseta}, and \ref{fig:looseoptfakespt}
\begin{figure}[ht]
    \centering
    \includegraphics[width=8.0cm]{figs/looseopteffetaHS.png}
    \includegraphics[width=8.0cm]{figs/looseopteffetaPU.png}
    \caption{Hard-Scatter (left) and pileup (right) Efficiencies vs $\eta$ for the Loose Optimization. Ratio is with respect to the Loose working point, which is not visible due to the blue working point being identical.}
    \label{fig:looseopteffseta}
\end{figure}

\begin{figure}[ht]
    \centering
    \includegraphics[width=8.0cm]{figs/looseopteffptHS.png}
    \includegraphics[width=8.0cm]{figs/looseopteffptPU.png}
    \caption{Hard-Scatter (left) and pileup (right) Efficiencies vs $p_T$ for the Loose Optimization. Ratio is with respect to the Loose working point, which is not visible due to the blue working point being identical.}
    \label{fig:looseopteffspt}
\end{figure}

\begin{figure}[ht]
    \centering
    \includegraphics[width=8.0cm]{figs/looseoptfakeseta.png}
    \includegraphics[width=8.0cm]{figs/looseoptfakesetaUL.png}
    \caption{Regular (left) and Unlinked (right) Fake Rates vs $\eta$ for the Loose Optimization. Ratio is with respect to the Loose working point, which is not visible due to the blue working point being identical.}
    \label{fig:looseoptfakeseta}
\end{figure}

\begin{figure}[ht]
    \centering
    \includegraphics[width=8.0cm]{figs/looseoptfakespt.png}
    \includegraphics[width=8.0cm]{figs/looseoptfakesptUL.png}
    \caption{Regular (left) and Unlinked (right) Fake Rates vs $p_T$ for the Loose Optimization. Ratio is with respect to the Loose working point, which is not visible due to the blue working point being identical.}
    \label{fig:looseoptfakespt}
\end{figure}
%In an ATLAS note, use the appendices to include all the technical details of your work
%that are relevant for the ATLAS Collaboration only (e.g.\ dataset details, software release used).
%This information should be printed after the Bibliography.

\end{document}
